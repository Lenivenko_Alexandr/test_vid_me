package com.test.aslen.vidme.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.test.aslen.vidme.R;
import com.test.aslen.vidme.tools.CheckInternet;

import java.io.IOException;

public class VideoActivity extends AppCompatActivity {

    // Declare variables
    ProgressDialog pDialog;
    VideoView videoview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the layout from video_main.xml
        setContentView(R.layout.activity_video);
        // Find your VideoView in your video_main.xml layout
        videoview = (VideoView) findViewById(R.id.videoView);
        // Execute StreamVideo AsyncTask



        Uri videoUri = getIntent().getData();
        if (videoUri != null&&CheckInternet.isOnline(this)) {
            // Create a progressbar
            pDialog = new ProgressDialog(VideoActivity.this);
            // Set progressbar title
            pDialog.setTitle(getResources().getString(R.string.progress_dialog_title_buffering));
            // Set progressbar message
            pDialog.setMessage(getResources().getString(R.string.progress_dialog_massege_buffering));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            // Show progressbar
            pDialog.show();

            try {
                // Start the MediaController
                MediaController mediacontroller = new MediaController(
                        VideoActivity.this);
                mediacontroller.setAnchorView(videoview);

                videoview.setMediaController(mediacontroller);
                videoview.setVideoURI(videoUri);

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            videoview.requestFocus();
            videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                // Close the progress bar and play the video
                public void onPrepared(MediaPlayer mp) {
                    pDialog.dismiss();
                    videoview.start();
                }
            });
        }else{
            AlertDialog alertDialog = CheckInternet.getDialog(this);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alertDialog.show();

        }
    }
}
