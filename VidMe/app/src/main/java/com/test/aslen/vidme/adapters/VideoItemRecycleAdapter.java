package com.test.aslen.vidme.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.test.aslen.vidme.R;
import com.test.aslen.vidme.model.Video;

import java.util.List;

public class VideoItemRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final int EMPTY_VIEW_TYPE = 100;
    private final int LIST_ITEM_TYPE = 101;


    private final Context mContext;
    private List<Video> mVideos;
    private LayoutInflater mInflater;

    private OnItemVideoClickListener mVideoClickListener;



    public VideoItemRecycleAdapter(Context context, List<Video> videos) {
        mVideos = videos;
        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType){
            case LIST_ITEM_TYPE:
                // create a new item video view

                View item = mInflater.inflate(R.layout.card_veiw_item_video, parent, false);
                // set the view's size, margins, paddings and layout parameters
                ListViewHolder listView = new ListViewHolder(item);
                return listView;


            case EMPTY_VIEW_TYPE:
                // create a new empty view

                View empty = mInflater.inflate(R.layout.no_data_veiw_item_video, parent, false);
                // set the view's size, margins, paddings and layout parameters
                EmptyViewHolder emptyView = new EmptyViewHolder(empty);
                return emptyView;

        }
        return null;

    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case LIST_ITEM_TYPE:
                // - get element from your dataset at this position
                // - replace the contents of the view with that element
                ListViewHolder listHolder = (ListViewHolder) holder;
                final Video video = mVideos.get(position);
                listHolder.videoNameTextView.setText(video.getTitle());
                listHolder.likesTextView.setText(getLikesText(video.getLikesCount()));

                Picasso.with(mContext)
                        .load(Uri.parse(video.getThumbnailUrl()))
                        .error(android.R.drawable.stat_notify_error)
                        .into(listHolder.thumbnails);

                break;


            case EMPTY_VIEW_TYPE:
                EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
        }


    }

    // return quantity of like plus text "like" or "likes" depends of quantity
    private String getLikesText(Integer likesCount) {

            if(likesCount>1){
                return likesCount + " likes";
            }else {
                return likesCount + " like";
            }

    }

    @Override
    public int getItemCount() {
        if(mVideos==null){
            return 1;
        }
        return mVideos.size();
    }


    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);

        if(position==0&mVideos == null){
            return EMPTY_VIEW_TYPE;
        }

        return LIST_ITEM_TYPE;
    }

    public void addAll(List<Video> mVideoList) {
        mVideos = mVideoList;
        notifyDataSetChanged();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        TextView videoNameTextView;
        TextView likesTextView;

        ImageView thumbnails;


        public ListViewHolder(View itemView) {
            super(itemView);
            videoNameTextView = (TextView) itemView.findViewById(R.id.videoNameTextViewItem);
            likesTextView = (TextView) itemView.findViewById(R.id.likesTextViewItem);

            thumbnails = (ImageView) itemView.findViewById(R.id.videoImageView);

            View rootLayout = itemView.findViewById(R.id.card_view);
            rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mVideoClickListener.onStartVideoClickListener(v,mVideos.get(getAdapterPosition()));
                }
            });

        }


    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {


        View updateButton;


        public EmptyViewHolder(View itemView) {
            super(itemView);
            updateButton =  itemView.findViewById(R.id.updateButton);

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mVideoClickListener.onUpdateList();
                }
            });

        }


    }



    public void setOnItemVideoClickListener(OnItemVideoClickListener videoClickListener){
        mVideoClickListener = videoClickListener;
    }
}
