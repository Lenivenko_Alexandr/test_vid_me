package com.test.aslen.vidme.api;



import com.test.aslen.vidme.model.TokenResponse;
import com.test.aslen.vidme.model.VideoList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    // type new or featured or feed

    @GET("/videos/{type}?limit=10")
    Call<VideoList> getVideo(
            @Path("type") String videoType,
            @Query("token") String token
    );

    // type new or featured or feed

    @GET("/videos/{type}?limit=10")
    Call<VideoList> getVideo(
            @Path("type") String videoType,
            @Query("offset") int offset,
            @Query("token") String token
    );

    /*@Multipart
    @FormUrlEncoded
    @POST("/auth/create")
    Call<Auth> getTokenAccess(@Body TokenRequest tokenRequest);*/


    @Headers("AccessToken: 23f5de5658ee499095de6856c870d73c")
    @FormUrlEncoded
    @POST("/auth/create")
    Call<TokenResponse> getTokenAccess(
            @Field("username") String username,
            @Field("password") String password
    );

    /*//login
    @POST("/videos/feed")
    Call<VideoList> getFeedVideo();
*/
    /*//login ?AccessToken=63dadca3b3aa4903860437634aa739a7
    @GET("/videos/feed?AccessToken={token}")
    Call<VideoList> getFeedVideo(@Path("token") String token);*/
    
}
