package com.test.aslen.vidme.adapters;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.test.aslen.vidme.R;
import com.test.aslen.vidme.fragments.ListVideoFragment;
import com.test.aslen.vidme.fragments.LoginFragment;
import com.test.aslen.vidme.fragments.RootFragment;
import com.test.aslen.vidme.tools.SessionManager;

import java.util.HashMap;
import java.util.Map;

public class ViaScreenPagerAdapter extends FragmentStatePagerAdapter {


    private final Context mContext;
    private final FragmentManager mFragmentManager;
    private String [] mArrTitle;
    private Map<String,RootFragment> mFragmentMap;

    public ViaScreenPagerAdapter(Context context, FragmentManager fm, String [] arrTitle) {
        super(fm);
        mFragmentManager = fm;
        mFragmentMap = new HashMap<>();
        mArrTitle = arrTitle;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        if(mArrTitle[position].equals(mContext.getString(R.string.FEED))&&!new SessionManager(mContext).isLoggedIn()) {
            LoginFragment loginFragment = new LoginFragment();
            mFragmentMap.put(mArrTitle[position],loginFragment);
            return loginFragment;
        }
        ListVideoFragment listVideoFragmen = new ListVideoFragment();
        listVideoFragmen.setParam(mArrTitle[position].toLowerCase());
        mFragmentMap.put(mArrTitle[position],listVideoFragmen);
        return listVideoFragmen;
    }

    @Override
    public int getCount() {
        return mArrTitle.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return mArrTitle[position];
    }



    public Fragment getFragment(String field) {

        return mFragmentMap.get(field);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mFragmentMap.remove(mArrTitle[position]);
    }



    /*public Fragment getFragment(int position) {
        return mFragmentMap.get(mArrTitle[position]);
    }*/
}
