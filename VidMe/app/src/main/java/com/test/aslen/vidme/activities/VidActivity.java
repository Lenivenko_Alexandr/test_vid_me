package com.test.aslen.vidme.activities;

import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.test.aslen.vidme.R;
import com.test.aslen.vidme.adapters.ViaScreenPagerAdapter;
import com.test.aslen.vidme.fragments.LoginFragment;
import com.test.aslen.vidme.fragments.RootFragment;
import com.test.aslen.vidme.tools.CheckInternet;
import com.test.aslen.vidme.tools.SessionManager;

public class VidActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ViaScreenPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vid);

            if(!CheckInternet.isOnline(this)){
                AlertDialog alertDialog = CheckInternet.getDialog(this);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO: ?
                    }
                });
                alertDialog.show();
            }



        mViewPager = (ViewPager) findViewById(R.id.viewPagerVidActivity);

        View view =  getLayoutInflater().inflate(R.layout.item_layout_tab, null);

        final TabLayout tabLayout = (TabLayout) view.findViewById(R.id.layoutTabStripForVidActivity);
        String [] arrTitleForTabLayout = getResources().getStringArray(R.array.tabs);



        mPagerAdapter = new ViaScreenPagerAdapter(this,getSupportFragmentManager(),arrTitleForTabLayout) ;
        mViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mToolbar.addView(view,param);


        //set correct tab
        if(new SessionManager(this).isLoggedIn()){
            mViewPager.setCurrentItem(0);
        }else {
            mViewPager.setCurrentItem(2);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                RootFragment fragment = (RootFragment) mPagerAdapter.getFragment(getResources().getStringArray(R.array.tabs)[tab.getPosition()]);
                //if fragment !=null -> reload date
                //else create new
                if(fragment!=null){
                    fragment.reloadData();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_menu.xml file.
        // This adds menu items to the app bar.
        if(new SessionManager(VidActivity.this).isLoggedIn()){
            getMenuInflater().inflate(R.menu.menu, menu);
        }

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Logout" menu option
            case R.id.action_log_out:

                logout();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);

    }

    //This method hides and shows menu in toolBar.
    //State dependences of login or logout user
    public void changeOptionsMenuState(){
        mToolbar.getMenu().clear();
        onCreateOptionsMenu(mToolbar.getMenu());

          if(!(new SessionManager(this)).isLoggedIn()){

            Fragment fragment =   mPagerAdapter.getFragment("FEED");
              if(fragment!=null){
                  getSupportFragmentManager().beginTransaction().detach(fragment)
                          .add(mPagerAdapter.getItem(2),"FEED").commit();
              }

            mViewPager.setCurrentItem(0);
        }else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.loginFragment,mPagerAdapter.getItem(2)).addToBackStack(null).commit();
        }


    }

    private void logout() {

        new SessionManager(this).logoutUser();

        changeOptionsMenuState();
    }




}
