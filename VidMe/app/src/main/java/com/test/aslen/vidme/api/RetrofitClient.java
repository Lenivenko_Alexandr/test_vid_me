package com.test.aslen.vidme.api;



import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    /*URL*/
    private static final String ROOT_URL = "https://api.vid.me";

    /*Get RETROFIT instance*/
    private static Retrofit getRetrofitInstance(){
        return new Retrofit.Builder().baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    /*Get API Service*/


    public static ApiService getApiService(){
        return getRetrofitInstance().create(ApiService.class);
    }
}
