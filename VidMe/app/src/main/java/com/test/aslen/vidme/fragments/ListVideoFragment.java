package com.test.aslen.vidme.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.aslen.vidme.R;
import com.test.aslen.vidme.activities.VideoActivity;
import com.test.aslen.vidme.adapters.EndlessRecyclerOnScrollListener;
import com.test.aslen.vidme.adapters.OnItemVideoClickListener;
import com.test.aslen.vidme.adapters.VideoItemRecycleAdapter;
import com.test.aslen.vidme.api.ApiService;
import com.test.aslen.vidme.api.RetrofitClient;
import com.test.aslen.vidme.model.Video;
import com.test.aslen.vidme.model.VideoList;
import com.test.aslen.vidme.tools.CheckInternet;
import com.test.aslen.vidme.tools.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListVideoFragment extends RootFragment implements OnItemVideoClickListener {

    private List<Video> mVideoList;
    List<Video> mTempList = null;
    private VideoItemRecycleAdapter mRecycleAdapter;
    private RecyclerView mVideoListRecycler;

    private View mProgressBar;
    SwipeRefreshLayout mSwipeContainer;





    public ListVideoFragment() {
        mParam = "";
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_video, container, false);


        //Show progressBar by the time when data upload
        mProgressBar = view.findViewById(R.id.loadingDataProgressBar);
        //mProgressBar.setVisibility(View.VISIBLE);

        mSwipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.videoListFragment);
        // Setup refresh listener which triggers new data loading
        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                loadVideo(0);
            }
        });

        //find out RecyclerView adapter and match it together
        mVideoListRecycler = (RecyclerView) view.findViewById(R.id.videoRecyclerView);

        mRecycleAdapter = new VideoItemRecycleAdapter(getContext(),mVideoList);
        //set custom Listener
        mRecycleAdapter.setOnItemVideoClickListener(ListVideoFragment.this);

        //load
        loadVideo();

        mVideoListRecycler.setAdapter(mRecycleAdapter);






        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mVideoListRecycler.setLayoutManager(layoutManager);

        mVideoListRecycler.setOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int total) {
                Log.d("My", "End" + total);

                mProgressBar.setVisibility(View.VISIBLE);
                loadVideo(total);

            }
        });




        return view;
    }


    private void loadVideo(int total){

        if(!CheckInternet.isOnline(getContext())){
            AlertDialog alertDialog = CheckInternet.getDialog(getContext());
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    mSwipeContainer.setRefreshing(false);
                }
            });
            alertDialog.show();
        }

        ApiService apiService = RetrofitClient.getApiService();

        Call<VideoList> videosListCall = apiService.getVideo(mParam,total,new SessionManager(getContext()).getUserToken());

        videosListCall.enqueue(new Callback<VideoList>() {
            @Override
            public void onResponse(Call<VideoList> call, Response<VideoList> response) {
                if(response.isSuccessful()){
                    //hide progressBar
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mSwipeContainer.setRefreshing(false);
                    mTempList = response.body().getVideos();

                    mVideoList.addAll(mTempList);
                    mRecycleAdapter.notifyDataSetChanged();

                }


            }

            @Override
            public void onFailure(Call<VideoList> call, Throwable t) {
                //hide progressBar
                mProgressBar.setVisibility(View.INVISIBLE);
                mSwipeContainer.setRefreshing(false);
                Log.e(ListVideoFragment.class.getName(),t.getMessage());

            }
        });

    }

    public void loadVideo(){

        //mProgressBar.setVisibility(View.VISIBLE);

        mSwipeContainer.setRefreshing(true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                ApiService apiService = RetrofitClient.getApiService();

                int offset = 0;

                Call<VideoList> videosListCall = apiService.getVideo(mParam,offset,new SessionManager(getContext()).getUserToken());

                videosListCall.enqueue(new Callback<VideoList>() {
                    @Override
                    public void onResponse(Call<VideoList> call, Response<VideoList> response) {
                        if(response.isSuccessful()){
                            //hide progressBar
                            mSwipeContainer.setRefreshing(false);
                            mProgressBar.setVisibility(View.INVISIBLE);


                            mVideoList = response.body().getVideos();

                            mRecycleAdapter.addAll(mVideoList);


                            mSwipeContainer.setRefreshing(false);
                        }


                    }

                    @Override
                    public void onFailure(Call<VideoList> call, Throwable t) {
                        //hide progressBar
                        mSwipeContainer.setRefreshing(false);
                        mProgressBar.setVisibility(View.INVISIBLE);
                        Log.e(ListVideoFragment.class.getName(),t.getMessage());

                    }
                });

            }
        }).start();



    }

    @Override
    public void onStartVideoClickListener(View v, Video video) {
        Intent intent = new Intent(getActivity(), VideoActivity.class);
        intent.setData(Uri.parse(video.getCompleteUrl()));
        startActivity(intent);
    }

    @Override
    public void onUpdateList() {
        mSwipeContainer.setRefreshing(true);
        //mProgressBar.setVisibility(View.VISIBLE);
        loadVideo();
    }

    @Override
    public void reloadData() {
        mSwipeContainer.setRefreshing(true);
        //mProgressBar.setVisibility(View.VISIBLE);
        loadVideo(0);
    }
}
