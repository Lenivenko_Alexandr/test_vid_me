package com.test.aslen.vidme.adapters;


import android.view.View;

import com.test.aslen.vidme.model.Video;

public interface OnItemVideoClickListener {
    void onStartVideoClickListener(View v, Video video);
    void onUpdateList();
}
