package com.test.aslen.vidme.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.test.aslen.vidme.R;
import com.test.aslen.vidme.activities.VidActivity;
import com.test.aslen.vidme.api.ApiService;
import com.test.aslen.vidme.api.RetrofitClient;
import com.test.aslen.vidme.model.TokenResponse;
import com.test.aslen.vidme.model.User;
import com.test.aslen.vidme.tools.CheckInternet;
import com.test.aslen.vidme.tools.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends RootFragment {


/*    private boolean mNameHasChanged =false;
    private boolean mPasswordHasChanged =false;*/



    private EditText mUserNameEditText;
    private EditText mPasswordEditText;


    //private View mProgressBar;

    private TextWatcher mUserTextChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mUserNameEditText.setError(null);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private TextWatcher mPasswordTextChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mPasswordEditText.setError(null);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Login Button
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        //EditText fields: password and userName
        // Find all relevant views that we will need to read user input from
        mUserNameEditText = (EditText) view.findViewById(R.id.usernameEditText);
        mPasswordEditText = (EditText) view.findViewById(R.id.passwordEditText);


        //progressBar
        //mProgressBar = view.findViewById(R.id.loginProgressBar);

        //set onTouchListener
        mUserNameEditText.addTextChangedListener(mUserTextChangeListener);
        mPasswordEditText.addTextChangedListener(mPasswordTextChangeListener);



        View loginButton = view.findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if no internet connection give user message
                if(!CheckInternet.isOnline(getContext())){
                    AlertDialog alertDialog = CheckInternet.getDialog(getContext());
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO: ?
                        }
                    });
                    alertDialog.show();
                }


                String userName = mUserNameEditText.getText().toString().trim();
                String password = mPasswordEditText.getText().toString().trim();


                if(TextUtils.isEmpty(userName)
                        || TextUtils.isEmpty(password)
                ){
                    // Check which fields haven't been filled

                    if(TextUtils.isEmpty(password)){
                        mPasswordEditText.setError("Filling password, please");
                        mPasswordEditText.requestFocus();

//                        mPasswordTouchListener.onTouch(null,null);
                    }
                    if(TextUtils.isEmpty(userName)){
                        mUserNameEditText.setError("Filling user name, please");
                        mUserNameEditText.requestFocus();

//                        mUserNameTouchListener.onTouch(null,null);
                    }

                }else {
                    //show progressBar
                    //mProgressBar.setVisibility(View.VISIBLE);

                    //try to login
                    ApiService apiService = RetrofitClient.getApiService();

                    //Create object User type and set password and userName from appropriate EditText
                    User user = new User();
                    user.setPassword(password);
                    user.setUsername(userName);
                    Call<TokenResponse> call = apiService.getTokenAccess(user.getUsername(),user.getPassword());

                    call.enqueue(new Callback<TokenResponse>() {
                        @Override
                        public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                            responseProcessing(response);

                            //hide progressBar
                            //mProgressBar.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onFailure(Call<TokenResponse> call, Throwable t) {
                            Log.e(LoginFragment.class.getName(),t.getMessage());

                            //hide progressBar
                            //mProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        });

        // Inflate the layout for this fragment
        return view;
    }










    /*
    * Method processes date from response.
    * In case if response.isSuccessful()== true (it means code == 200),
    * token will be saved in appropriate field of SharedPreference.
    * In case if response.isSuccessful()== false,
    * will be shown appropriate dialog message
    */


    private void responseProcessing(Response<TokenResponse> response) {
        if(response.isSuccessful()){
            TokenResponse body = response.body();
            String token = body.getAuth().getToken();
            new SessionManager(getContext()).createLoginSession(token);
            ((VidActivity)getActivity()).changeOptionsMenuState();

        }else {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();

                alertDialog.setTitle(getResources().getString(R.string.login_error_dialog_title));
                alertDialog.setMessage(getResources().getString(R.string.login_error_dialog_massage));
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        clearField();

                    }
                });

                alertDialog.show();
            }catch(Exception e)
            {
                Log.d(LoginFragment.class.getName(), "Show Dialog: "+e.getMessage());
            }
        }
    }

    //       mUserNameEditText and mPasswordEditText will be cleared
    private void clearField() {

        mUserNameEditText.getText().clear();
        mPasswordEditText.getText().clear();

        mUserNameEditText.requestFocus();
    }




}
