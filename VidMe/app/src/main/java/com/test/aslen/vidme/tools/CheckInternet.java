package com.test.aslen.vidme.tools;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

import com.test.aslen.vidme.R;
import com.test.aslen.vidme.activities.VidActivity;

/**
 * Class gives state of internet
 */

public class CheckInternet {

    /*Method return true if device has internet connection*/
    public static boolean isOnline(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean flag = netInfo != null && netInfo.isConnectedOrConnecting();
        if(!flag){
            //getDialog(context);
        }

        return flag;
    }

    public static AlertDialog getDialog(final Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(context.getResources().getString(R.string.internet_connection_dialog_title));
        alertDialog.setMessage(context.getResources().getString(R.string.internet_connection_dialog_message));
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        /*alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });*/

        return alertDialog;
    }

    /*public static boolean hasPermissions(Context context, String... permissions)
    {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null)
        {
            for (String permission : permissions)
            {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
                {
                    return false;
                }
            }
        }
        return true;
    }*/
}
