package com.test.aslen.vidme.tools;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "userToken";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String STATUS = "status";
    private static final String STATUS_COLOR = "status_color";



    // User token (make variable public to access from outside)
    public static final String KEY_TOKEN = "token";


    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    /**
     * Create login session
     * */
    public void createLoginSession(String token){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing token in pref
        editor.putString(KEY_TOKEN, token);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            //           Intent i = new Intent(_context, LoginScreen.class);
            //           // Closing all the Activities
            //           i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //
            //           // Add new Flag to start new Activity
            //           i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //
            //           // Staring Login Activity
            //           _context.startActivity(i);
        }

    }

    /**
     * Get stored session data
     * */
    public String getUserToken(){

        // return token
        return pref.getString(KEY_TOKEN, null);
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        //       Intent i = new Intent(_context, LoginScreen.class);
        //       // Closing all the Activities
        //       i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //
        //       // Add new Flag to start new Activity
        //       i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //
        //       // Staring Login Activity
        //       _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
// Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
